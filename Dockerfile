#
# Apache 2.4 builder image
#
FROM gitlab-registry.oit.duke.edu/devops/containers/centos/8:latest

ENV HTTPD_SERVER_NAME=localhost \
    S2I_SCRIPTS_PATH=/usr/libexec/s2i

ENV PATH="${S2I_SCRIPTS_PATH}:${PATH}"

LABEL io.openshift.expose-services="8080:http,8443:https"
LABEL io.openshift.s2i.scripts-url="image://${S2I_SCRIPTS_PATH}"

RUN yum -y install httpd mod_ssl \
    && yum clean all

RUN sed -i 's/^Listen .*/Listen 8080/' /etc/httpd/conf/httpd.conf

RUN [[ -f /etc/pki/tls/certs/localhost.crt ]] || /usr/libexec/httpd-ssl-gencerts

COPY ./httpd-cfg/ /etc/httpd/conf.d/
COPY ./s2i/bin/ ${S2I_SCRIPTS_PATH}

RUN mkdir -p /usr/local/etc/httpd \
    && chgrp -R 0 /etc/httpd/logs /run/httpd /var/www \
    && chmod -R g=u /etc/httpd/logs /run/httpd /var/www /usr/local/etc/httpd \
    && chmod -R g+r /etc/pki/tls/certs /etc/pki/tls/private

VOLUME /var/www/html /usr/local/etc/httpd

EXPOSE 8080 8443

USER 1001

HEALTHCHECK CMD [ "healthcheck" ]

# https://httpd.apache.org/docs/2.4/stopping.html#gracefulstop
STOPSIGNAL SIGWINCH

CMD [ "usage" ]
